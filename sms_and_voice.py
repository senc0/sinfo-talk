# -*- coding: utf-8 -*-
import json

from twilio.rest import Client


secrets = None
barata = None
jpaiva = None
volunteer = None

with open(".secrets.json") as json_file:
    secrets = json.loads(json_file.read())

    # our kind volunteers
    jpaiva = secrets.get("jpaiva")      # joao paiva
    barata = secrets.get("rsbarata")    # ricardo barata

# Instantiate the Twilio client
# =============================
client = Client(secrets.get("account_sid"),
                secrets.get("auth_token"))


# Send a text message
# ===================

# Purchase a number from Twilio
sms_numbers = client.available_phone_numbers("PT") \
   .mobile.list(sms_enabled=True)

sms_number = client.incoming_phone_numbers \
   .create(phone_number=sms_numbers[0].phone_number)

print "This should be stored somewhere, like a datastore."
print sms_number.sid

# or ...

# Get a previously purchased phone from Twilio

# number_sid = "PN33845868b5fdbcb6a82dd172a17cb1eb" # previously aquired
# my_number = client.incoming_phone_numbers(number_sid).fetch()

# print "This should be stored somewhere, like a datastore."
# print my_number.phone_number

# Send the text
client.messages.create(
   to=volunteer,
   from_=sms_number.phone_number,
   body="Hey there, this is a test, can you tell me on Slack if you got it?"
)


# Voice call
# ==========

# Aquire the voice capable phone
voice_numbers = client.available_phone_numbers("PT") \
   .local.list(voice_enabled=True)

voice_number = client.incoming_phone_numbers \
   .create(phone_number=voice_numbers[0].phone_number)

print "Aquired number, this should be stored in a datastore"
print voice_number.sid

# Send a voice call audio stolen from Twilio with 💖
call = client.calls.create(
   to=volunteer,
   from_=voice_number.phone_number,
   url="http://demo.twilio.com/docs/voice.xml"
)

print "Call is ongoing"
print call.sid
